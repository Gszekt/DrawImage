﻿namespace DrawImage
{
    partial class DrawImageForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DrawImageForm));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.文件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.打开ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.保存ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.编辑ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.放大ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.缩小ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.拖动ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.工具ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.标注ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.标注ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.折线标注ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.擦除标注ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.检测裂缝ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.测量裂缝ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.图片滤波ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.图像处理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.图片转换ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tIFF转JPEGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.其他转TIFFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.图片裁剪ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.图像拼接ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.关于ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.关于ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.显示和修补ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.显示测量结果ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.清楚图像ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.数据录入ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton10 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton9 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.iconList = new System.Windows.Forms.ImageList(this.components);
            this.contextMenuTreeViewView = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.显示原图ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.显示裂缝ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.长度测量ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.删除图像ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.CancelAll = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.imagesTreeView = new System.Windows.Forms.TreeView();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.picturePanel = new System.Windows.Forms.Panel();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.menuStrip.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.contextMenuTreeViewView.SuspendLayout();
            this.picturePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.文件ToolStripMenuItem,
            this.编辑ToolStripMenuItem,
            this.工具ToolStripMenuItem,
            this.图像处理ToolStripMenuItem,
            this.关于ToolStripMenuItem,
            this.数据录入ToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Padding = new System.Windows.Forms.Padding(8, 3, 0, 3);
            this.menuStrip.Size = new System.Drawing.Size(803, 27);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // 文件ToolStripMenuItem
            // 
            this.文件ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.打开ToolStripMenuItem,
            this.保存ToolStripMenuItem});
            this.文件ToolStripMenuItem.Name = "文件ToolStripMenuItem";
            this.文件ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.文件ToolStripMenuItem.Text = "文件";
            // 
            // 打开ToolStripMenuItem
            // 
            this.打开ToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.打开ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.打开ToolStripMenuItem.Name = "打开ToolStripMenuItem";
            this.打开ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.打开ToolStripMenuItem.Text = "打开";
            this.打开ToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.打开ToolStripMenuItem.Click += new System.EventHandler(this.打开ToolStripMenuItem_Click);
            // 
            // 保存ToolStripMenuItem
            // 
            this.保存ToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.保存ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.保存ToolStripMenuItem.Name = "保存ToolStripMenuItem";
            this.保存ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.保存ToolStripMenuItem.Text = "保存";
            this.保存ToolStripMenuItem.Click += new System.EventHandler(this.保存ToolStripMenuItem_Click);
            // 
            // 编辑ToolStripMenuItem
            // 
            this.编辑ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.放大ToolStripMenuItem,
            this.缩小ToolStripMenuItem,
            this.拖动ToolStripMenuItem});
            this.编辑ToolStripMenuItem.Name = "编辑ToolStripMenuItem";
            this.编辑ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.编辑ToolStripMenuItem.Text = "编辑";
            // 
            // 放大ToolStripMenuItem
            // 
            this.放大ToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.放大ToolStripMenuItem.Name = "放大ToolStripMenuItem";
            this.放大ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.放大ToolStripMenuItem.Text = "放大";
            this.放大ToolStripMenuItem.Click += new System.EventHandler(this.放大ToolStripMenuItem_Click);
            // 
            // 缩小ToolStripMenuItem
            // 
            this.缩小ToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.缩小ToolStripMenuItem.Name = "缩小ToolStripMenuItem";
            this.缩小ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.缩小ToolStripMenuItem.Text = "缩小";
            this.缩小ToolStripMenuItem.Click += new System.EventHandler(this.缩小ToolStripMenuItem_Click);
            // 
            // 拖动ToolStripMenuItem
            // 
            this.拖动ToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.拖动ToolStripMenuItem.Name = "拖动ToolStripMenuItem";
            this.拖动ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.拖动ToolStripMenuItem.Text = "拖动";
            this.拖动ToolStripMenuItem.Click += new System.EventHandler(this.拖动ToolStripMenuItem_Click);
            // 
            // 工具ToolStripMenuItem
            // 
            this.工具ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.标注ToolStripMenuItem,
            this.擦除标注ToolStripMenuItem,
            this.检测裂缝ToolStripMenuItem,
            this.测量裂缝ToolStripMenuItem,
            this.图片滤波ToolStripMenuItem});
            this.工具ToolStripMenuItem.Name = "工具ToolStripMenuItem";
            this.工具ToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.工具ToolStripMenuItem.Text = "标记工具";
            // 
            // 标注ToolStripMenuItem
            // 
            this.标注ToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.标注ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.标注ToolStripMenuItem1,
            this.折线标注ToolStripMenuItem});
            this.标注ToolStripMenuItem.Name = "标注ToolStripMenuItem";
            this.标注ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.标注ToolStripMenuItem.Text = "标注";
            // 
            // 标注ToolStripMenuItem1
            // 
            this.标注ToolStripMenuItem1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.标注ToolStripMenuItem1.Name = "标注ToolStripMenuItem1";
            this.标注ToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.标注ToolStripMenuItem1.Text = "标注";
            this.标注ToolStripMenuItem1.Click += new System.EventHandler(this.标注ToolStripMenuItem1_Click);
            // 
            // 折线标注ToolStripMenuItem
            // 
            this.折线标注ToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.折线标注ToolStripMenuItem.Name = "折线标注ToolStripMenuItem";
            this.折线标注ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.折线标注ToolStripMenuItem.Text = "折线标注";
            this.折线标注ToolStripMenuItem.Click += new System.EventHandler(this.折线标注ToolStripMenuItem_Click);
            // 
            // 擦除标注ToolStripMenuItem
            // 
            this.擦除标注ToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.擦除标注ToolStripMenuItem.Name = "擦除标注ToolStripMenuItem";
            this.擦除标注ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.擦除标注ToolStripMenuItem.Text = "擦除标注";
            this.擦除标注ToolStripMenuItem.Click += new System.EventHandler(this.擦除标注ToolStripMenuItem_Click);
            // 
            // 检测裂缝ToolStripMenuItem
            // 
            this.检测裂缝ToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.检测裂缝ToolStripMenuItem.Name = "检测裂缝ToolStripMenuItem";
            this.检测裂缝ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.检测裂缝ToolStripMenuItem.Text = "检测裂缝";
            this.检测裂缝ToolStripMenuItem.Click += new System.EventHandler(this.检测裂缝ToolStripMenuItem_Click);
            // 
            // 测量裂缝ToolStripMenuItem
            // 
            this.测量裂缝ToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.测量裂缝ToolStripMenuItem.Name = "测量裂缝ToolStripMenuItem";
            this.测量裂缝ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.测量裂缝ToolStripMenuItem.Text = "测量裂缝";
            this.测量裂缝ToolStripMenuItem.Click += new System.EventHandler(this.测量裂缝ToolStripMenuItem_Click);
            // 
            // 图片滤波ToolStripMenuItem
            // 
            this.图片滤波ToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.图片滤波ToolStripMenuItem.Name = "图片滤波ToolStripMenuItem";
            this.图片滤波ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.图片滤波ToolStripMenuItem.Text = "图片滤波";
            this.图片滤波ToolStripMenuItem.Click += new System.EventHandler(this.图片滤波ToolStripMenuItem_Click);
            // 
            // 图像处理ToolStripMenuItem
            // 
            this.图像处理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.图片转换ToolStripMenuItem,
            this.图片裁剪ToolStripMenuItem,
            this.图像拼接ToolStripMenuItem});
            this.图像处理ToolStripMenuItem.Name = "图像处理ToolStripMenuItem";
            this.图像处理ToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.图像处理ToolStripMenuItem.Text = "图像处理";
            // 
            // 图片转换ToolStripMenuItem
            // 
            this.图片转换ToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.图片转换ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tIFF转JPEGToolStripMenuItem,
            this.其他转TIFFToolStripMenuItem});
            this.图片转换ToolStripMenuItem.Name = "图片转换ToolStripMenuItem";
            this.图片转换ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.图片转换ToolStripMenuItem.Text = "图片转换";
            // 
            // tIFF转JPEGToolStripMenuItem
            // 
            this.tIFF转JPEGToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tIFF转JPEGToolStripMenuItem.Name = "tIFF转JPEGToolStripMenuItem";
            this.tIFF转JPEGToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.tIFF转JPEGToolStripMenuItem.Text = "TIFF转JPEG";
            this.tIFF转JPEGToolStripMenuItem.Click += new System.EventHandler(this.tIFF转JPEGToolStripMenuItem_Click);
            // 
            // 其他转TIFFToolStripMenuItem
            // 
            this.其他转TIFFToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.其他转TIFFToolStripMenuItem.Name = "其他转TIFFToolStripMenuItem";
            this.其他转TIFFToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.其他转TIFFToolStripMenuItem.Text = "其他转TIFF";
            this.其他转TIFFToolStripMenuItem.Click += new System.EventHandler(this.其它转TIFFToolStripMenuItem_Click);
            // 
            // 图片裁剪ToolStripMenuItem
            // 
            this.图片裁剪ToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.图片裁剪ToolStripMenuItem.Name = "图片裁剪ToolStripMenuItem";
            this.图片裁剪ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.图片裁剪ToolStripMenuItem.Text = "图片裁剪";
            this.图片裁剪ToolStripMenuItem.Click += new System.EventHandler(this.图片裁剪ToolStripMenuItem_Click);
            // 
            // 图像拼接ToolStripMenuItem
            // 
            this.图像拼接ToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.图像拼接ToolStripMenuItem.Name = "图像拼接ToolStripMenuItem";
            this.图像拼接ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.图像拼接ToolStripMenuItem.Text = "图像拼接";
            this.图像拼接ToolStripMenuItem.Click += new System.EventHandler(this.图像拼接ToolStripMenuItem_Click);
            // 
            // 关于ToolStripMenuItem
            // 
            this.关于ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.关于ToolStripMenuItem1,
            this.显示和修补ToolStripMenuItem,
            this.显示测量结果ToolStripMenuItem,
            this.清楚图像ToolStripMenuItem});
            this.关于ToolStripMenuItem.Name = "关于ToolStripMenuItem";
            this.关于ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.关于ToolStripMenuItem.Text = "显示";
            // 
            // 关于ToolStripMenuItem1
            // 
            this.关于ToolStripMenuItem1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.关于ToolStripMenuItem1.Name = "关于ToolStripMenuItem1";
            this.关于ToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.关于ToolStripMenuItem1.Text = "显示原图";
            this.关于ToolStripMenuItem1.Click += new System.EventHandler(this.显示原图ToolStripMenuItem1_Click);
            // 
            // 显示和修补ToolStripMenuItem
            // 
            this.显示和修补ToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.显示和修补ToolStripMenuItem.Name = "显示和修补ToolStripMenuItem";
            this.显示和修补ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.显示和修补ToolStripMenuItem.Text = "显示裂缝";
            this.显示和修补ToolStripMenuItem.Click += new System.EventHandler(this.显示裂缝ToolStripMenuItem_Click);
            // 
            // 显示测量结果ToolStripMenuItem
            // 
            this.显示测量结果ToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.显示测量结果ToolStripMenuItem.Name = "显示测量结果ToolStripMenuItem";
            this.显示测量结果ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.显示测量结果ToolStripMenuItem.Text = "显示测量结果";
            this.显示测量结果ToolStripMenuItem.Click += new System.EventHandler(this.显示测量结果ToolStripMenuItem_Click);
            // 
            // 清楚图像ToolStripMenuItem
            // 
            this.清楚图像ToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.清楚图像ToolStripMenuItem.Name = "清楚图像ToolStripMenuItem";
            this.清楚图像ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.清楚图像ToolStripMenuItem.Text = "清除标记";
            this.清楚图像ToolStripMenuItem.Click += new System.EventHandler(this.清除图像ToolStripMenuItem_Click);
            // 
            // 数据录入ToolStripMenuItem
            // 
            this.数据录入ToolStripMenuItem.Name = "数据录入ToolStripMenuItem";
            this.数据录入ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.数据录入ToolStripMenuItem.Text = "数据";
            this.数据录入ToolStripMenuItem.Click += new System.EventHandler(this.数据录入ToolStripMenuItem_Click);
            // 
            // toolStrip
            // 
            this.toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton3,
            this.toolStripSeparator1,
            this.toolStripButton10,
            this.toolStripButton7,
            this.toolStripSeparator2,
            this.toolStripButton4,
            this.toolStripButton9,
            this.toolStripSeparator3,
            this.toolStripButton5,
            this.toolStripSeparator4,
            this.toolStripButton8,
            this.toolStripButton6});
            this.toolStrip.Location = new System.Drawing.Point(0, 27);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(803, 40);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Enabled = false;
            this.toolStripButton1.Image = global::DrawImage.Properties.Resources.grab;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(36, 37);
            this.toolStripButton1.Text = "拖动";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Enabled = false;
            this.toolStripButton2.Image = global::DrawImage.Properties.Resources.zoomIn;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(36, 37);
            this.toolStripButton2.Text = "放大";
            this.toolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Enabled = false;
            this.toolStripButton3.Image = global::DrawImage.Properties.Resources.zoomOut;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(36, 37);
            this.toolStripButton3.Text = "缩小";
            this.toolStripButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 40);
            // 
            // toolStripButton10
            // 
            this.toolStripButton10.Enabled = false;
            this.toolStripButton10.Image = global::DrawImage.Properties.Resources.replay;
            this.toolStripButton10.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton10.Name = "toolStripButton10";
            this.toolStripButton10.Size = new System.Drawing.Size(36, 37);
            this.toolStripButton10.Text = "撤销";
            this.toolStripButton10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton10.Click += new System.EventHandler(this.toolStripButton10_Click);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.Enabled = false;
            this.toolStripButton7.Image = global::DrawImage.Properties.Resources.rubber;
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(36, 37);
            this.toolStripButton7.Text = "擦除";
            this.toolStripButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 40);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.Enabled = false;
            this.toolStripButton4.Image = global::DrawImage.Properties.Resources.line;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(36, 37);
            this.toolStripButton4.Text = "标注";
            this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolStripButton9
            // 
            this.toolStripButton9.Enabled = false;
            this.toolStripButton9.Image = global::DrawImage.Properties.Resources.polyline;
            this.toolStripButton9.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton9.Name = "toolStripButton9";
            this.toolStripButton9.Size = new System.Drawing.Size(36, 37);
            this.toolStripButton9.Text = "折线";
            this.toolStripButton9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton9.ToolTipText = "折线标注";
            this.toolStripButton9.Click += new System.EventHandler(this.toolStripButton9_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 40);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.Enabled = false;
            this.toolStripButton5.Image = global::DrawImage.Properties.Resources.ruler;
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(36, 37);
            this.toolStripButton5.Text = "测量";
            this.toolStripButton5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 40);
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.Enabled = false;
            this.toolStripButton8.Image = global::DrawImage.Properties.Resources.delete;
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(72, 37);
            this.toolStripButton8.Text = "删除列表项";
            this.toolStripButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton8.Click += new System.EventHandler(this.toolStripButton8_Click);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.Enabled = false;
            this.toolStripButton6.Image = global::DrawImage.Properties.Resources.save;
            this.toolStripButton6.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(36, 37);
            this.toolStripButton6.Text = "保存";
            this.toolStripButton6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton6.Click += new System.EventHandler(this.toolStripButton6_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 431);
            this.statusStrip1.Margin = new System.Windows.Forms.Padding(0, 2, 0, 2);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip1.Size = new System.Drawing.Size(803, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(68, 17);
            this.toolStripStatusLabel1.Text = "放大倍数：";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(68, 17);
            this.toolStripStatusLabel2.Text = "显示状态：";
            // 
            // iconList
            // 
            this.iconList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("iconList.ImageStream")));
            this.iconList.TransparentColor = System.Drawing.Color.Transparent;
            this.iconList.Images.SetKeyName(0, "folder_close.ico");
            this.iconList.Images.SetKeyName(1, "folder_open.ico");
            this.iconList.Images.SetKeyName(2, "image.ico");
            // 
            // contextMenuTreeViewView
            // 
            this.contextMenuTreeViewView.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.显示原图ToolStripMenuItem,
            this.显示裂缝ToolStripMenuItem,
            this.长度测量ToolStripMenuItem,
            this.删除图像ToolStripMenuItem,
            this.SelectAll,
            this.CancelAll});
            this.contextMenuTreeViewView.Name = "contextMenuListView";
            this.contextMenuTreeViewView.Size = new System.Drawing.Size(125, 136);
            // 
            // 显示原图ToolStripMenuItem
            // 
            this.显示原图ToolStripMenuItem.Name = "显示原图ToolStripMenuItem";
            this.显示原图ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.显示原图ToolStripMenuItem.Text = "显示原图";
            this.显示原图ToolStripMenuItem.Click += new System.EventHandler(this.显示原图ContextMenuItem_Click);
            // 
            // 显示裂缝ToolStripMenuItem
            // 
            this.显示裂缝ToolStripMenuItem.Name = "显示裂缝ToolStripMenuItem";
            this.显示裂缝ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.显示裂缝ToolStripMenuItem.Text = "显示裂缝";
            this.显示裂缝ToolStripMenuItem.Click += new System.EventHandler(this.显示裂缝ContextMenuItem_Click);
            // 
            // 长度测量ToolStripMenuItem
            // 
            this.长度测量ToolStripMenuItem.Name = "长度测量ToolStripMenuItem";
            this.长度测量ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.长度测量ToolStripMenuItem.Text = "长度测量";
            this.长度测量ToolStripMenuItem.Click += new System.EventHandler(this.长度测量ContextMenuItem_Click);
            // 
            // 删除图像ToolStripMenuItem
            // 
            this.删除图像ToolStripMenuItem.Name = "删除图像ToolStripMenuItem";
            this.删除图像ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.删除图像ToolStripMenuItem.Text = "删除图像";
            this.删除图像ToolStripMenuItem.Click += new System.EventHandler(this.删除图像ContextMenuItem_Click);
            // 
            // SelectAll
            // 
            this.SelectAll.Name = "SelectAll";
            this.SelectAll.Size = new System.Drawing.Size(124, 22);
            this.SelectAll.Text = "全选";
            this.SelectAll.Click += new System.EventHandler(this.全选ContextMenuItem_Click);
            // 
            // CancelAll
            // 
            this.CancelAll.Name = "CancelAll";
            this.CancelAll.Size = new System.Drawing.Size(124, 22);
            this.CancelAll.Text = "取消全选";
            this.CancelAll.Click += new System.EventHandler(this.取消全选ContextMenuItem_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "Jpeg (*.jpg)|*.jpg";
            // 
            // imagesTreeView
            // 
            this.imagesTreeView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.imagesTreeView.CheckBoxes = true;
            this.imagesTreeView.Dock = System.Windows.Forms.DockStyle.Left;
            this.imagesTreeView.Font = new System.Drawing.Font("微软雅黑 Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.imagesTreeView.ImageIndex = 0;
            this.imagesTreeView.ImageList = this.iconList;
            this.imagesTreeView.ItemHeight = 24;
            this.imagesTreeView.LabelEdit = true;
            this.imagesTreeView.Location = new System.Drawing.Point(0, 67);
            this.imagesTreeView.Name = "imagesTreeView";
            this.imagesTreeView.SelectedImageIndex = 0;
            this.imagesTreeView.ShowLines = false;
            this.imagesTreeView.Size = new System.Drawing.Size(176, 364);
            this.imagesTreeView.TabIndex = 4;
            this.imagesTreeView.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.imagesTreeView_AfterCheck);
            this.imagesTreeView.BeforeCollapse += new System.Windows.Forms.TreeViewCancelEventHandler(this.imagesTreeView_BeforeCollapse);
            this.imagesTreeView.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.imagesTreeView_BeforeExpand);
            this.imagesTreeView.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.imagesTreeView_NodeMouseClick);
            this.imagesTreeView.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.imagesTreeView_NodeMouseDoubleClick);
            // 
            // splitter1
            // 
            this.splitter1.Cursor = System.Windows.Forms.Cursors.SizeWE;
            this.splitter1.Location = new System.Drawing.Point(176, 67);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(6, 364);
            this.splitter1.TabIndex = 5;
            this.splitter1.TabStop = false;
            // 
            // picturePanel
            // 
            this.picturePanel.Controls.Add(this.pictureBox);
            this.picturePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picturePanel.Location = new System.Drawing.Point(182, 67);
            this.picturePanel.Margin = new System.Windows.Forms.Padding(4, 3, 3, 3);
            this.picturePanel.Name = "picturePanel";
            this.picturePanel.Size = new System.Drawing.Size(621, 364);
            this.picturePanel.TabIndex = 6;
            // 
            // pictureBox
            // 
            this.pictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox.Location = new System.Drawing.Point(0, 0);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(100, 50);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            // 
            // DrawImageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(803, 453);
            this.Controls.Add(this.picturePanel);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.imagesTreeView);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.menuStrip);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.MainMenuStrip = this.menuStrip;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "DrawImageForm";
            this.Text = "DrawImage";
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.contextMenuTreeViewView.ResumeLayout(false);
            this.picturePanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem 文件ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 打开ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 保存ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 编辑ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 放大ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 缩小ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 拖动ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 工具ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 标注ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 擦除标注ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 关于ToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripMenuItem 关于ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem 检测裂缝ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 测量裂缝ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 图片滤波ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 显示和修补ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 显示测量结果ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 清楚图像ToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ContextMenuStrip contextMenuTreeViewView;
        private System.Windows.Forms.ToolStripMenuItem SelectAll;
        private System.Windows.Forms.ToolStripMenuItem CancelAll;
		private System.Windows.Forms.ToolStripMenuItem 显示原图ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem 显示裂缝ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem 长度测量ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem 删除图像ToolStripMenuItem;
		private System.Windows.Forms.ImageList iconList;
		private System.Windows.Forms.ToolStripButton toolStripButton9;
		private System.Windows.Forms.ToolStripButton toolStripButton10;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripMenuItem 图像处理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 图片转换ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 图片裁剪ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 图像拼接ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 数据录入ToolStripMenuItem;
        private System.Windows.Forms.TreeView imagesTreeView;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel picturePanel;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.ToolStripMenuItem tIFF转JPEGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 其他转TIFFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 标注ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 折线标注ToolStripMenuItem;
    }
}

