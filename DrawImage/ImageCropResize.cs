﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Windows.Forms;
namespace DrawImage
{
    public partial class ImageCropResize : Form
    {
        #region 字段声明
		Rectangle rcLT; // left-top
		Rectangle rcRT; // right-top
		Rectangle rcLB; // left-bottom
		Rectangle rcRB; // right-bottom
		Rectangle rcMT; // middle-top
		Rectangle rcMB; // middle-bottom
		Rectangle rcML; // middle-left
		Rectangle rcMR; // middle-right
		Rectangle CropRect;
		Rectangle rcOld, rcNew;
		Rectangle rcOriginal;
		Rectangle rcBegin;
		SolidBrush BrushRect;
		HatchBrush BrushRectSmall;
		Color BrushColor;
		int AlphaBlend;
		int nSize;
		int nWd;
		int nHt;
		int nResizeRT;
		int nResizeBL;
		int nResizeLT;
		int nResizeRB;
		int nResizeMT;
		int nResizeMB;
		int nResizeML;
		int nResizeMR;
		int nThatsIt;
		int nCropRect;
		Point ptOld;
		Point ptNew;
		private object scrollH;
		private object scrollV;
		private int zoomRatio=2;
		private bool isMaxZoom;
		private bool isMinZoom;
		Size ImageOriginalSize;
		#endregion 字段声明

		public ImageCropResize()
        {
            InitializeComponent();
            InitializeCropRectangle();
        }

		/// <summary>
		/// 初始化裁剪矩形块
		/// </summary>
		public void InitializeCropRectangle()
        {
            AlphaBlend = 100;
            nSize = 16;
            nWd = 50;
            nHt = 50;
            nThatsIt = 0;
            nResizeRT = 0;
            nResizeBL = 0;
            nResizeLT = 0;
            nResizeRB = 0;
			nResizeMT = 0;
			nResizeMB = 0;
			nResizeML = 0;
			nResizeMR = 0;
			BrushColor = Color.White;
            BrushRect = new SolidBrush(Color.FromArgb(AlphaBlend, BrushColor.R, BrushColor.G, BrushColor.B));
            BrushColor = Color.Red;
            BrushRectSmall = new HatchBrush(HatchStyle.Percent50, Color.FromArgb(192, BrushColor.R, BrushColor.G, BrushColor.B));
            ptOld = new Point(0, 0);
            rcBegin = new Rectangle();
            rcOriginal = new Rectangle(0, 0, 0, 0);
            rcLT = new Rectangle(0, 0, nSize, nSize);
            rcRT = new Rectangle(0, 0, nSize, nSize);
            rcLB = new Rectangle(0, 0, nSize, nSize);
            rcRB = new Rectangle(0, 0, nSize, nSize);
			rcMT = new Rectangle(0, 0, nSize, nSize);
			rcMB = new Rectangle(0, 0, nSize, nSize);
			rcML = new Rectangle(0, 0, nSize, nSize);
			rcMR = new Rectangle(0, 0, nSize, nSize);
            rcOld = CropRect = new Rectangle(0, 0, nWd, nHt);
            AdjustResizeRects();
        }

        /// <summary>
        /// 调整拖拽矩形块的位置
        /// </summary>
        private void AdjustResizeRects()
        {
            rcLT.X = CropRect.Left;
            rcLT.Y = CropRect.Top;
            rcRT.X = CropRect.Right - rcRT.Width;
            rcRT.Y = CropRect.Top;
            rcLB.X = CropRect.Left;
            rcLB.Y = CropRect.Bottom - rcLB.Height;
            rcRB.X = CropRect.Right - rcRB.Width;
            rcRB.Y = CropRect.Bottom - rcRB.Height;
            rcMT.X = CropRect.Left + CropRect.Width / 2 - rcMT.Width / 2;
            rcMT.Y = CropRect.Top;
            rcMB.X = CropRect.Left + CropRect.Width / 2 - rcMB.Width / 2;
            rcMB.Y = CropRect.Bottom - rcMB.Height;
            rcML.X = CropRect.Left;
            rcML.Y = CropRect.Top + CropRect.Height / 2 - rcML.Height / 2;
            rcMR.X = CropRect.Right - rcMR.Width;
            rcMR.Y = CropRect.Top + CropRect.Height / 2 - rcMR.Height / 2;
        }

        /// <summary>
        /// 调整裁剪矩形块的位置和大小
        /// </summary>
        private void AdjustCropRect() {
			if (CropRect.Width > pictureBox.Width) {
				CropRect.Width = pictureBox.Width;
			}
			if (CropRect.Height > pictureBox.Height) {
				CropRect.Height = pictureBox.Height;
			}
			if (CropRect.Right > pictureBox.Right - 1) {
				CropRect.Offset(pictureBox.Right - CropRect.Right, 0);
			}
			if (CropRect.Bottom > pictureBox.Bottom - 1) {
				CropRect.Offset(0, -(CropRect.Bottom - pictureBox.Bottom));
			}
		}

		/// <summary>
		/// 打开图片按钮响应
		/// </summary>
		private void button1_Click(object sender, EventArgs e)
        {
            //==========打开文件对话框
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Multiselect = false,//设置一次打开多个文件
                RestoreDirectory = true,//下次打开对话框是否定位到上次打开的目录
                //过滤文件类型
                // openFileDialog.Filter = "图片文件 (*.bmp)|*.bmp|所有文件 (*.*)|*.*";
                Filter = "所有文件 (*.*)|*.*",
                //FilterIndex 与 Filter 关联对应，用于设置默认显示的文件类型
                FilterIndex = 1//默认是1，则默认显示的文件类型为*.txt；如果设置为2，则默认显示的文件类型是*.*
            };
            Cursor = Cursors.AppStarting;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                Image img = Image.FromFile(openFileDialog.FileName);
                pictureBox.Image = img;
                pictureBox.Size = img.Size;
				ImageOriginalSize = img.Size;
				pictureBox.Refresh();
				
                CropRect.Width = 400;
                CropRect.Height = 400;
                AdjustResizeRects();
                
                pictureBox.Update();
                button2.Enabled = true;
                button4.Enabled = true;
            }
            
            pictureBox.Refresh();
            // pictureBox_Paint(null,null);
            Cursor = Cursors.Default;
        }

        /// <summary>
        /// 保存按钮响应
        /// </summary>
        private void button2_Click(object sender, EventArgs e)
        {
            Bitmap bmp = null;
            var tempPoint = pointToPicture(new Point(CropRect.X, CropRect.Y));
            Rectangle ScaledCropRect = new Rectangle
            {
                X = tempPoint.X,
                Y = tempPoint.Y,
                Width = CropRect.Width * ImageOriginalSize.Width / pictureBox.Width,
                Height = CropRect.Height * ImageOriginalSize.Width / pictureBox.Width
            };
            if (saveFileDialog2.ShowDialog() == DialogResult.OK) {
                try {
                    bmp = (Bitmap)CropImage(pictureBox.Image, ScaledCropRect);
                    saveJpeg(saveFileDialog2.FileName, bmp, 100);
                }
                catch (Exception ex) {
                    MessageBox.Show(ex.Message, "btnOK_Click()");
                }
            }
            bmp?.Dispose();
        }

        /// <summary>
        /// 放大按钮响应
        /// </summary>
        private void button3_Click(object sender, EventArgs e)
        {
            ZoomIn();
        }

        /// <summary>
        /// 缩小按钮响应
        /// </summary>
        private void button4_Click(object sender, EventArgs e)
        {
            ZoomOut();
        }

        private void pictureBox_Paint(object sender, PaintEventArgs e)
        {
            if (pictureBox.Image == null)
            {
                // display checkerboard
                bool xGrayBox = true;
                int backgroundX = 0;
                while (backgroundX < pictureBox.Width)
                {
                    int backgroundY = 0;
                    bool yGrayBox = xGrayBox;
                    while (backgroundY < pictureBox.Height)
                    {
                        int recWidth = (int)((backgroundX + 50 > pictureBox.Width) ? pictureBox.Width - backgroundX : 50);
                        int recHeight = (int)((backgroundY + 50 > pictureBox.Height) ? pictureBox.Height - backgroundY : 50);
                        e.Graphics.FillRectangle(((Brush)(yGrayBox ? Brushes.LightGray : Brushes.Gainsboro)), backgroundX, backgroundY, recWidth + 2, recHeight + 2);
                        backgroundY += 50;
                        yGrayBox = !yGrayBox;
                    }
                    backgroundX += 50;
                    xGrayBox = !xGrayBox;
                }
            }
            else
            {
                // main crop box 
                e.Graphics.FillRectangle((BrushRect), CropRect);
                // corner drag boxes
                e.Graphics.FillRectangle((BrushRectSmall), rcLT);
                e.Graphics.FillRectangle((BrushRectSmall), rcRT);
                e.Graphics.FillRectangle((BrushRectSmall), rcLB);
                e.Graphics.FillRectangle((BrushRectSmall), rcRB);
				e.Graphics.FillRectangle((BrushRectSmall), rcMT);
				e.Graphics.FillRectangle((BrushRectSmall), rcMB);
				e.Graphics.FillRectangle((BrushRectSmall), rcML);
				e.Graphics.FillRectangle((BrushRectSmall), rcMR);
				AdjustResizeRects();
            }
            base.OnPaint(e);
        }

        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            Point pt = new Point(e.X, e.Y);
            rcOriginal = CropRect;
            rcBegin = CropRect;
            if (rcRB.Contains(pt)) {
                rcOld = new Rectangle(CropRect.X, CropRect.Y, CropRect.Width, CropRect.Height);
                rcNew = rcOld;
                nResizeRB = 1;
            }
            else if (rcLB.Contains(pt)) {
                rcOld = new Rectangle(CropRect.X, CropRect.Y, CropRect.Width, CropRect.Height);
                rcNew = rcOld;
                nResizeBL = 1;
            }
            else if (rcRT.Contains(pt)) {
                rcOld = new Rectangle(CropRect.X, CropRect.Y, CropRect.Width, CropRect.Height);
                rcNew = rcOld;
                nResizeRT = 1;
            }
            else if (rcLT.Contains(pt)) {
                rcOld = new Rectangle(CropRect.X, CropRect.Y, CropRect.Width, CropRect.Height);
                rcNew = rcOld;
                nResizeLT = 1;
            }
            else if (rcMT.Contains(pt)) {
                rcOld = new Rectangle(CropRect.X, CropRect.Y, CropRect.Width, CropRect.Height);
                rcNew = rcOld;
                nResizeMT = 1;
            }
            else if (rcMB.Contains(pt)) {
                rcOld = new Rectangle(CropRect.X, CropRect.Y, CropRect.Width, CropRect.Height);
                rcNew = rcOld;
                nResizeMB = 1;
            }
            else if (rcML.Contains(pt)) {
                rcOld = new Rectangle(CropRect.X, CropRect.Y, CropRect.Width, CropRect.Height);
                rcNew = rcOld;
                nResizeML = 1;
            }
            else if (rcMR.Contains(pt)) {
                rcOld = new Rectangle(CropRect.X, CropRect.Y, CropRect.Width, CropRect.Height);
                rcNew = rcOld;
                nResizeMR = 1;
            }
            else if (CropRect.Contains(pt)) {
                nResizeBL = nResizeLT = nResizeRB = nResizeRT = 0;
                nCropRect = 1;
                ptNew = ptOld = pt;
            }
            nThatsIt = 1;
            base.OnMouseDown(e);
        }

        private void pictureBox_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
        }

		private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
			if (pictureBox.Image == null)
				return;
			Point pt = new Point(e.X, e.Y);
   //         #region 设置光标形状
			//if (rcLT.Contains(pt))
			//	Cursor = Cursors.SizeNWSE;
			//else if (rcRT.Contains(pt))
			//	Cursor = Cursors.SizeNESW;
			//else if (rcLB.Contains(pt))
			//	Cursor = Cursors.SizeNESW;
			//else if (rcRB.Contains(pt))
			//	Cursor = Cursors.SizeNWSE;
			//else if (rcMT.Contains(pt))
			//	Cursor = Cursors.SizeNS;
			//else if (rcMB.Contains(pt))
			//	Cursor = Cursors.SizeNS;
			//else if (rcML.Contains(pt))
			//	Cursor = Cursors.SizeWE;
			//else if (rcMR.Contains(pt))
			//	Cursor = Cursors.SizeWE;
			//else if (CropRect.Contains(pt))
			//	Cursor = Cursors.SizeAll;
			//else
			//	Cursor = Cursors.Default;
   //         #endregion
			if (e.Button == MouseButtons.Left) {
				if (nResizeRB == 1) {
					rcNew.X = CropRect.X;
					rcNew.Y = CropRect.Y;
					rcNew.Width = pt.X - rcNew.Left;
					rcNew.Height = pt.Y - rcNew.Top;
					if (rcNew.X > rcNew.Right) {
						rcNew.Offset(-nWd, 0);
						if (rcNew.X < 0)
							rcNew.X = 0;
					}
					if (rcNew.Y > rcNew.Bottom) {
						rcNew.Offset(0, -nHt);
						if (rcNew.Y < 0)
							rcNew.Y = 0;
					}
					
					DrawDragRect(e);
					rcOld = CropRect = rcNew;
					Cursor = Cursors.SizeNWSE;
				}
				else if (nResizeBL == 1) {
					rcNew.X = pt.X;
					rcNew.Y = CropRect.Y;
					rcNew.Width = CropRect.Right - pt.X;
					rcNew.Height = pt.Y - rcNew.Top;
					if (rcNew.X > rcNew.Right) {
						rcNew.Offset(nWd, 0);
						if (rcNew.Right > ClientRectangle.Width)
							rcNew.Width = ClientRectangle.Width - rcNew.X;
					}
					if (rcNew.Y > rcNew.Bottom) {
						rcNew.Offset(0, -nHt);
						if (rcNew.Y < 0)
							rcNew.Y = 0;
					}
					
					DrawDragRect(e);
					rcOld = CropRect = rcNew;
					Cursor = Cursors.SizeNESW;
				}
				else if (nResizeRT == 1) {
					rcNew.X = CropRect.X;
					rcNew.Y = pt.Y;
					rcNew.Width = pt.X - rcNew.Left;
					rcNew.Height = CropRect.Bottom - pt.Y;
					if (rcNew.X > rcNew.Right) {
						rcNew.Offset(-nWd, 0);
						if (rcNew.X < 0)
							rcNew.X = 0;
					}
					if (rcNew.Y > rcNew.Bottom) {
						rcNew.Offset(0, nHt);
						if (rcNew.Bottom > ClientRectangle.Height)
							rcNew.Y = ClientRectangle.Height - rcNew.Height;
					}
					
					DrawDragRect(e);
					rcOld = CropRect = rcNew;
					Cursor = Cursors.SizeNESW;
				}
				else if (nResizeLT == 1) {
					rcNew.X = pt.X;
					rcNew.Y = pt.Y;
					rcNew.Width = CropRect.Right - pt.X;
					rcNew.Height = CropRect.Bottom - pt.Y;
					if (rcNew.X > rcNew.Right) {
						//    rcNew.Offset(nWd, 0);
						if (rcNew.Right > ClientRectangle.Width)
							rcNew.Width = ClientRectangle.Width - rcNew.X;
					}
					if (rcNew.Y > rcNew.Bottom) {
						rcNew.Offset(0, nHt);
						if (rcNew.Bottom > ClientRectangle.Height)
							rcNew.Height = ClientRectangle.Height - rcNew.Y;
					}
					
					DrawDragRect(e);
					rcOld = CropRect = rcNew;
					Cursor = Cursors.SizeNWSE;
				}
				else if (nResizeMT == 1) {
					rcNew.X = CropRect.X;
					rcNew.Y = pt.Y;
					rcNew.Width = CropRect.Width;
					rcNew.Height = CropRect.Bottom - pt.Y;
					if (rcNew.X > rcNew.Right) {
					}
					if (rcNew.Y > rcNew.Bottom) {
					}
					
					DrawDragRect(e);
					rcOld = CropRect = rcNew;
					Cursor = Cursors.SizeNS;
				}
				else if (nResizeMB == 1) {
					rcNew.X = CropRect.X;
					rcNew.Y = CropRect.Y;
					rcNew.Width = CropRect.Width;
					rcNew.Height = pt.Y - rcNew.Top;
					if (rcNew.X > rcNew.Right) {
					}
					if (rcNew.Y > rcNew.Bottom) {
					}
					
					DrawDragRect(e);
					rcOld = CropRect = rcNew;
					Cursor = Cursors.SizeNS;
				}
				else if (nResizeML == 1) {
					rcNew.X = pt.X;
					rcNew.Y = CropRect.Y;
					rcNew.Width = CropRect.Right - pt.X;
					rcNew.Height = CropRect.Height;
					if (rcNew.X > rcNew.Right) {
					}
					if (rcNew.Y > rcNew.Bottom) {
					}
					
					DrawDragRect(e);
					rcOld = CropRect = rcNew;
					Cursor = Cursors.SizeWE;
				}
				else if (nResizeMR == 1) {
					rcNew.X = CropRect.X;
					rcNew.Y = CropRect.Y;
					rcNew.Width = pt.X - rcNew.Left;
					rcNew.Height = CropRect.Height;
					if (rcNew.X > rcNew.Right) {
					}
					if (rcNew.Y > rcNew.Bottom) {
					}
					DrawDragRect(e);
					rcOld = CropRect = rcNew;
					Cursor = Cursors.SizeWE;
				}
				else if (nCropRect == 1) //Moving the rectangle
				{
					ptNew = pt;
					int dx = ptNew.X - ptOld.X;
					int dy = ptNew.Y - ptOld.Y;
					CropRect.Offset(dx, dy);
					//Console.WriteLine(CropRect.Location);
					//rcNew = CropRect;
					DrawDragRect(e);
					ptOld = ptNew;
                    Cursor = Cursors.SizeAll;
				}
				
				AdjustResizeRects();
				pictureBox.Update();
			}
			base.OnMouseMove(e);
		}

		private void pictureBox_MouseUp(object sender, MouseEventArgs e)
        {
			if (nThatsIt == 0)
				return;
			nCropRect = 0;
			nResizeRB = 0;
			nResizeBL = 0;
			nResizeRT = 0;
			nResizeLT = 0;
			nResizeMT = 0;
			nResizeMB = 0;
			nResizeML = 0;
			nResizeMR = 0;
			if (CropRect.Width <= 0 || CropRect.Height <= 0)
				CropRect = rcOriginal;
			if (CropRect.X < 0)
				CropRect.X = 0;
			if (CropRect.Y < 0)
				CropRect.Y = 0;
			//AdjustCropRect();
			// need to add logic for portrait mode of crop box in this
			// area
			// now that the crop box position is established
			// force it to the proper aspect ratio
			// and scale it
			//if (CropRect.Width > CropRect.Height)
			//{
			//    CropRect.Height = (int)(CropRect.Width / CropAspectRatio);
			//}
			//else
			//{
			//    CropRect.Width = (int)(CropRect.Height * CropAspectRatio);
			//}
			AdjustResizeRects();
			pictureBox.Refresh();
			base.OnMouseUp(e);
			nWd = rcNew.Width;
			nHt = rcNew.Height;
			rcBegin = rcNew;
		}

		private void pictureBox_MouseEnter(object sender, EventArgs e) {
			this.pictureBox.Focus();
		}

		bool iszooming = false;
		private void pictureBox_MouseWheel(object sender, MouseEventArgs e)
        {
			if (iszooming) {
				return;
			}
			iszooming = true;
			if (e.Delta >= 0) {
				ZoomIn();
			}
			else {
				ZoomOut();
			}
			iszooming = false;
		}

		private void ImageCropResize_Load(object sender, EventArgs e)
        {
            panel2.AutoScroll = true;
            panel2.AutoSize = false;
            panel2.HorizontalScroll.Value = 0;
            panel2.VerticalScroll.Value = 0;
            pictureBox.Dock = DockStyle.None;
            pictureBox.SizeMode = PictureBoxSizeMode.AutoSize;
            pictureBox.Location = new Point(0, 0);
           
        }

		/// <summary>
		/// 拖动图片
		/// </summary>
        private void DrawDragRect(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                AdjustResizeRects();
                pictureBox.Invalidate();
            }
        }

		/// <summary>
		/// 剪切图片
		/// </summary>
		private static Image CropImage(Image img, Rectangle cropArea)
        {
           // MessageBox.Show("cropArea width="+cropArea.Width+"heigt="+cropArea.Height);
            try
            {
                Bitmap bmpImage = new Bitmap(img);
                Bitmap bmpCrop = bmpImage.Clone(cropArea, bmpImage.PixelFormat);
                return bmpCrop;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "CropImage()");
            }
            return null;
        }

		private ImageCodecInfo getEncoderInfo(string mimeType)
        {
            // Get image codecs for all image formats
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            // Find the correct image codec
            for (int i = 0; i < codecs.Length; i++)
                if (codecs[i].MimeType == mimeType)
                    return codecs[i];
            return null;
        }

		private void saveJpeg(string path, Bitmap img, long quality)
        {
            // Encoder parameter for image quality
            EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, (long)quality);
            // Jpeg image codec
            ImageCodecInfo jpegCodec = getEncoderInfo("image/jpeg");
            if (jpegCodec == null)
            {
                MessageBox.Show("Can't find JPEG encoder?", "saveJpeg()");
                return;
            }
            EncoderParameters encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;
            img.Save(path, jpegCodec, encoderParams);
        }

		/// <summary>
		/// 放大
		/// </summary>
		private void ZoomIn()
        {   // 放大
			if (pictureBox.Image != null && pictureBox.Height < ImageOriginalSize.Height
                && pictureBox.Width < ImageOriginalSize.Width && !isMaxZoom) {
				panel2.AutoScroll = true;
				scrollH = panel2.HorizontalScroll.Value;
				scrollV = panel2.VerticalScroll.Value;
				panel2.HorizontalScroll.Value = 0;
				panel2.VerticalScroll.Value = 0;
				int picBoxHeight = pictureBox.Height;//=临时保存用
				int picBoxWidth = pictureBox.Width;//=临时保存用
				pictureBox.Location = new Point(0, 0);
				pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
				pictureBox.Dock = DockStyle.None;
				pictureBox.Height = (int)(picBoxHeight * zoomRatio);//
				pictureBox.Width = (int)(picBoxWidth * zoomRatio);
				isMinZoom = false;//=放大之后就又可以缩小了
				int MultipleMagnification = (ImageOriginalSize.Height / pictureBox.Height) * 2;
				if (MultipleMagnification == 2) {
					MultipleMagnification = 1;
                    button3.Enabled = false;
                    isMaxZoom = true;
				}
                // 调整裁剪矩形的大小和位置
                CropRect.Width *= zoomRatio;
                CropRect.Height *= zoomRatio;
                CropRect.X *= zoomRatio;
                CropRect.Y *= zoomRatio;
                AdjustCropRect();
                AdjustResizeRects();
				pictureBox.Refresh();
            }
		}

		/// <summary>
		/// 缩小
		/// </summary>
		private void ZoomOut() {
			if (pictureBox.Image != null && (!isMinZoom)) {
				panel2.AutoScroll = true;
				scrollH = panel2.HorizontalScroll.Value;
				scrollV = panel2.VerticalScroll.Value;
				int picBoxHeight = pictureBox.Height;//=临时保存用
				int picBoxWidth = pictureBox.Width;//=临时保存用
				panel2.HorizontalScroll.Value = 0;//=注释掉这两句将影响pictureBox的位置
				panel2.VerticalScroll.Value = 0;
				pictureBox.Location = new Point(0, 0);
				pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
				pictureBox.Dock = DockStyle.None;
				
				pictureBox.Height = (int)(picBoxHeight / zoomRatio);//
				pictureBox.Width = (int)(picBoxWidth / zoomRatio);
				isMaxZoom = false;//=缩小之后就又可以放大
                button3.Enabled = true;
                // 调整裁剪矩形的大小和位置
                CropRect.Width /= zoomRatio;
                CropRect.Height /= zoomRatio;
                CropRect.X /= zoomRatio;
                CropRect.Y /= zoomRatio;
                AdjustCropRect();
                AdjustResizeRects();
				pictureBox.Refresh();
            }
		}

        Point pointToPicture(Point screen) {
			Point ptPicture = new Point();
			ptPicture.X = screen.X * (ImageOriginalSize.Width / pictureBox.Width);
			ptPicture.Y = screen.Y * (ImageOriginalSize.Height / pictureBox.Height);
			return ptPicture;
		}
	}
}
