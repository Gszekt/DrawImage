﻿using System;
using System.Data.SQLite;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace DrawImage
{
    public partial class DataEnter : Form
    {
        public DataEnter()
        {
            InitializeComponent();
        }

        public DataEnter(DataGridViewRow row)
        {
            InitializeComponent();
            this.textBox1.Text = row.Cells[1].Value.ToString();
            this.textBox2.Text = row.Cells[2].Value.ToString();
            this.textBox3.Text = row.Cells[3].Value.ToString();
            this.textBox4.Text = row.Cells[4].Value.ToString();
            this.textBox5.Text = row.Cells[5].Value.ToString();
            this.textBox6.Text = row.Cells[6].Value.ToString();
            this.textBox7.Text = row.Cells[7].Value.ToString();
            this.textBox8.Text = row.Cells[8].Value.ToString();
            this.textBox9.Text = row.Cells[9].Value.ToString();
        }

        private void 确认Button_Click(object sender, EventArgs e)
        {
            var name = this.textBox1.Text.Trim();
            var id = this.textBox2.Text.Trim();
            var longitude = this.textBox3.Text.Trim();
            var lantitude = this.textBox4.Text.Trim();
            var altitude = this.textBox5.Text.Trim();
            var height = this.textBox6.Text.Trim();
            var distance = this.textBox7.Text.Trim();
            var angle = this.textBox8.Text.Trim();
            var position = this.textBox9.Text.Trim();
            int errCode = 0;
            if (Exists(id))
                errCode = UpdateData(name, id, longitude, lantitude, altitude, height, distance, angle, position);
            else
                errCode = InsertIntoDB(name, id, longitude, lantitude, altitude, height, distance, angle, position);
            if (errCode != 0) {
                MessageBox.Show($"输入数据有误！");
            }
            else {
                MessageBox.Show($"成功！");
            }
        }

        private static bool Exists(string id)
        {
            var sql = @"select 1 from images where id = @id";
            var parameter = new SQLiteParameter("@id", id);
            return DBHelperSQLite.Query(sql, parameter).Tables[0].Rows.Count > 0;
        }

        private static int UpdateData(string name, string id, string longitude, string lantitude, string altitude, string height, string distance, string angle, string position)
        {
            int errCode = 0;
            if (id == "") {
                MessageBox.Show("编号不能为空！");
                return -1;
            }

            var sql = @"update images
                        set name = @name,
                            longitude = @longitude,
                            latitude = @latitude,
                            altitude = @altitude,
                            height = @height,
                            distance = @distance,
                            angle = @angle,
                            position = @position
                        where id = @id";

            var parameters = new SQLiteParameter[]
            {
                new SQLiteParameter("@name",name),
                new SQLiteParameter("@id",id),
                new SQLiteParameter("@longitude",longitude),
                new SQLiteParameter("@latitude",lantitude),
                new SQLiteParameter("@altitude",altitude),
                new SQLiteParameter("@height",height),
                new SQLiteParameter("@distance",distance),
                new SQLiteParameter("@angle",angle),
                new SQLiteParameter("@position",position)
            };
            try {
                var ds = DBHelperSQLite.Query(sql, parameters);
            }
            catch (Exception) {
                errCode = 1;
            }
            return errCode;
        }

        private static int InsertIntoDB(string name, string id, string longitude, string lantitude, string altitude, string height, string distance, string angle, string position)
        {
            int errCode = 0;
            if (id == "") {
                MessageBox.Show("编号不能为空！");
                return -1;
            }

            var sql = @"insert into images
                        values(  @name,
		                        @id,
                                @longitude,
                                @latitude,
                                @altitude,
                                @height,
                                @distance,
                                @angle,
                                @position);";
            var parameters = new SQLiteParameter[]
            {
                new SQLiteParameter("@name",name),
                new SQLiteParameter("@id",id),
                new SQLiteParameter("@longitude",longitude),
                new SQLiteParameter("@latitude",lantitude),
                new SQLiteParameter("@altitude",altitude),
                new SQLiteParameter("@height",height),
                new SQLiteParameter("@distance",distance),
                new SQLiteParameter("@angle",angle),
                new SQLiteParameter("@position",position)
            };
            try {
                var ds = DBHelperSQLite.Query(sql, parameters);
            }
            catch (Exception) {
                errCode = 1;
            }
            return errCode;
        }
    }
}
