﻿using System;
using System.Windows.Forms;
namespace DrawImage
{
    public partial class InfoForm : Form
    {
        public System.Threading.Timer Thread_Time;
        private string InfoBody { get; set; }
        private string infoText;
        private int i = 0;

        public InfoForm(string _InfoBody)
        {
            InfoBody = _InfoBody;
            InitializeComponent();
        }
        private void DetectCrack_Load(object sender, EventArgs e)
        {
            infoText = InfoBody;
            Thread_Time = new System.Threading.Timer(Thread_Timer_Method, null, 0, 200);
        }
        void Thread_Timer_Method(object o)
        {
            i++;
            infoText = infoText + "。";
            if (6 == i) {
                infoText = InfoBody;
                i = 0;
            }
            this?.Invoke(new MethodInvoker(delegate {
                this.label1.Text = infoText;
                this.label1.Refresh();
            }));
        }
    }
}
